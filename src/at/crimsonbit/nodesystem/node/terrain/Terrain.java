package at.crimsonbit.nodesystem.node.terrain;

import at.crimsonbit.nodesystem.gui.node.GNode;
import at.crimsonbit.nodesystem.node.IGuiNodeType;
import javafx.scene.paint.Color;

public enum Terrain implements IGuiNodeType {

	DISPLACEMENT("Displacement Node"), DIALTE("Dilate Node"), ERODE("Erode Node");

	private String name;

	private Terrain(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public Class<? extends GNode> getCustomNodeClass() {
		return GNode.class;
	}

	@Override
	public Color getColor() {
		return Color.BLUEVIOLET;
	}

}
